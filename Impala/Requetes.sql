-----  dcli_profil_partic_actuel -----

-- Enlève les clients partis de caisse d'epargne avant 2012-12
create table temp.dcli_profil_partic_actuel stored as parquet as 
select 	numr_pers,
		code_civl,
		code_sexe,
		date_nais,
 		datediff(now(),cast(date_nais as timestamp))/365 as age_calcul,
		datediff(cast(date_sort_clnt as timestamp),cast(date_nais as timestamp))/365 as age_sortie,
		nb_entt_titl_simp_ouvr,
		nb_entt_titl_join_ouvr,
		date_arrv_clnt_res,
		date_sort_clnt,
		date_decs,
		code_csp,
		code_type_regm_matr,
		code_type_sitt_faml,
		code_segm_comp,
		code_ins_pays_nais,
		code_dept_nais,
		code_segm_frqn_clnt,
		nb_jour_ancn_clnt,
		code_segm_unvr_v2
from import.dcli_profil_partic where ((date_sort_clnt > '2012-12') OR (date_sort_clnt IS NULL)) AND (datediff(now(),cast(date_nais as timestamp))/365 between 15 AND 30)


-----  ids_profil_partic -----

-- Tables des ids uniques pour ne garder que les opérations des profils qui nous intéressent
create table temp.ids_profil_partic stored as parquet as select distinct(numr_pers) from temp.dcli_profil_partic_actuel


-----  rdwh_oprt_jeunes -----

-- Tables des opérations des jeunes entre 15 et 25 ans

create table temp.rdwh_oprt_jeunes stored as parquet as
select * from import.rdwh_oprt_finn_coll_hist where numr_pers in (select * from temp.ids_profil_partic)


-----  rdwh_oprt_jeunes_simpl -----

-- Tables des opérations des jeunes entre 15 et 25 ans avec seulement les colonnes intéressantes

create table temp.rdwh_oprt_jeunes_simpl stored as parquet as
select perd_arrt_info,
		code_prdt,
		code_annl_oprt,
		code_type_mode_finn,
		code_sens_imput,
		mt_brut_oprt_prs_ordr,
		code_forc_oprt,
		numr_pers
from temp.rdwh_oprt_jeunes


-----  rdwh_oprt_jeunes_simpl_agg -----

-- Tables des opérations des jeunes entre 15 et 25 ans aggrégées par mois, type, sens ..
create table temp.rdwh_oprt_jeunes_simpl_agg stored as parquet as
select  perd_arrt_info, 
		--code_type_mode_finn, 
		code_sens_imput,
		numr_pers,
		SUM(mt_brut_oprt_prs_ordr) as sum_mt_brut,
		COUNT(nullif(code_annl_oprt,'N')) as nb_annl_oprt,
		COUNT(nullif(code_forc_oprt,'N')) as nb_forc_oprt
from temp.rdwh_oprt_jeunes_simpl
group by perd_arrt_info, code_type_mode_finn, code_sens_imput,numr_pers

