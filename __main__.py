import pip


#pip.main(['install','./wheelhouse/pip-8.1.2-py2.py3-none-any.whl'])
#pip.main(['install','./wheelhouse/pytz-2016.4-py2.py3-none-any.whl'])
#pip.main(['install','./wheelhouse/six-1.10.0-py2.py3-none-any.whl'])
#pip.main(['install','./wheelhouse/cycler-0.10.0-py2.py3-none-any.whl'])
#pip.main(['install','./wheelhouse/pyparsing-2.1.4-py2.py3-none-any.whl'])
#pip.main(['install','./wheelhouse/python_dateutil-2.5.3-py2.py3-none-any.whl'])
#pip.main(['install','./wheelhouse/numpy-1.11.0-cp27-cp27mu-linux_x86_64.whl'])
#pip.main(['install','./wheelhouse/pandas-0.18.1-cp27-cp27mu-linux_x86_64.whl'])
#pip.main(['install','./wheelhouse/scipy-0.14.0-cp27-cp27mu-manylinux1_x86_64.whl'])
#pip.main(['install','./wheelhouse/scikit_learn-0.17.1-cp27-cp27mu-manylinux1_x86_64.whl'])

#pip.main(['install','./wheelhouse/docopt-0.6.2-py2.py3-none-any.whl'])
#pip.main(['install','./wheelhouse/requests-2.10.0-py2.py3-none-any.whl'])
#pip.main(['install','./wheelhouse/hdfs-2.0.9-py2-none-any.whl'])

import churn_detection
import sys
import pandas as pd

if __name__ == '__main__':

	hdfs_master = "hdfs://" + sys.argv[1]
	print(hdfs_master)
	nb_months_features = 6
	nb_months_label = 6
	features_list = ['sum_mt_brut']

	df_oprt = churn_detection.get_oprt_data(hdfs_master +'/user/hive/warehouse/temp.db/rdwh_oprt_jeunes_simpl_agg_txt/')
	df_profil = churn_detection.get_profil_data(hdfs_master + '/user/hive/warehouse/temp.db/dcli_profil_partic_actuel_txt/')

	df_oprt_pivot = pd.pivot_table(df_oprt, values=['sum_mt_brut','nb_annl_oprt'], aggfunc=sum, index=['numr_pers'], columns=['perd_arrt_info','code_sens_imput']).fillna(0)
	df_join = pd.merge(df_oprt_pivot, df_profil, how='left', left_index=True, right_index= True)
	churn_detection.predict_churn(df_join, nb_months_features,nb_months_label,features_list)
