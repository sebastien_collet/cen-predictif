#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import glob
import datetime
from dateutil.relativedelta import relativedelta
from sklearn.manifold import TSNE
pd.options.mode.chained_assignment = None
#import matplotlib.pyplot as plt

from sklearn.metrics import f1_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import StandardScaler
from sklearn import preprocessing
from sklearn.cross_validation import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.grid_search import GridSearchCV 
from sklearn.manifold import TSNE
from sklearn import tree
from sklearn.externals.six import StringIO
from hdfs import InsecureClient
#import xgboost as xgb

def get_oprt_data(path):
    client = InsecureClient('http://192.168.10.10:50070', user='hive')
    path = "/user/hive/warehouse/temp.db/rdwh_oprt_jeunes_simpl_agg_txt/"
    allFiles = client.list(path,status=True)
    allFiles = [x[0] for x in allFiles if x[1]['type'] == 'FILE']

    list_ = []
    for file_ in allFiles:
        with client.read(path+file_) as reader:
            frame = pd.read_csv(reader, sep=';', header=None, names=("perd_arrt_info","code_sens_imput","numr_pers","sum_mt_brut","nb_annl_oprt","nb_forc_oprt"))
        list_.append(frame)
    df_oprt = pd.concat(list_)
    return df_oprt

def get_profil_data(path):
    client = InsecureClient('http://192.168.10.10:50070', user='hive')
    path = "/user/hive/warehouse/temp.db/dcli_profil_partic_actuel_txt/"
    allFiles = client.list(path,status=True)
    allFiles = [x[0] for x in allFiles if x[1]['type'] == 'FILE']    
    list_ = []
    for file_ in allFiles:
        with client.read(path+file_) as reader:
            frame = pd.read_csv(reader, sep=';', header=None, index_col = 'numr_pers', names=("numr_pers","code_civl","code_sexe","date_nais","age_calcul","age_sortie","nb_entt_titl_simp_ouvr","nb_entt_titl_join_ouvr","date_arrv_clnt_res","date_sort_clnt","date_decs","code_csp","code_type_regm_matr","code_type_sitt_faml","code_segm_comp","code_ins_pays_nais","code_dept_nais","code_segm_frqn_clnt","nb_jour_ancn_clnt","code_segm_unvr_v2"))
        list_.append(frame)
    df_profil = pd.concat(list_)
    # Keep only relevant columns
    df_profil = df_profil[["code_sexe","date_nais","date_sort_clnt","date_decs"]]
    
    def dateparse(dates):
        if dates != '\\N':
            return pd.datetime.strptime(dates,'%Y-%m-%d %H:%M:%S.%f')
        else:
            return None
    
    df_profil["date_nais"]= df_profil.date_nais.map(dateparse)
    df_profil["date_sort_clnt"]= df_profil.date_sort_clnt.map(dateparse)
    df_profil["date_decs"]= df_profil.date_decs.map(dateparse)
    
    return df_profil

def get_dates_from_columns(dataframe,features_list):
    columns = list(dataframe)
    dates = list(zip(*columns))[1]
    dates_set = set()
    for date in dates:
        try:
            dates_set.add(datetime.datetime.strptime(str(date),"%Y%m"))
        except:
            pass
    return sorted(list(dates_set))

def create_features(date,feature_list):
    features = []
    if isinstance(feature_list,list) or isinstance(feature_list,tuple):
        for feature in feature_list:
            features.append(((feature,int(datetime.datetime.strftime(date,format ='%Y%m')),'C'),(feature,int(datetime.datetime.strftime(date,format ='%Y%m')),'D')))
    else:
        features.append(((feature_list,int(datetime.datetime.strftime(date,format ='%Y%m')),'C'),(feature_list,int(datetime.datetime.strftime(date,format ='%Y%m')),'D')))
    return features


def create_dataset(dataframe, nb_months_features, nb_months_label, sample_type, features_list):
    df = dataframe.copy()
    dates_df = get_dates_from_columns(df, features_list)
    dates_features = dates_df[0:nb_months_features]
    # Delete people that churn during the learning phase
    df = df[~((df.date_sort_clnt >= dates_features[0]) & (df.date_sort_clnt <= (dates_features[-1] + pd.offsets.MonthEnd(0))))]
    
    dates_begin_label = dates_features[-1] + relativedelta(months=1)
    dates_end_label = dates_begin_label + relativedelta(months=nb_months_label)
    df["churn"] = ((df.date_sort_clnt >= dates_begin_label) & (df.date_sort_clnt <= (dates_end_label + pd.offsets.MonthEnd(0)))).map(lambda x: 1 if x else 0)
    # Delete dead people
    df = df[df["date_decs"].isnull()]
    # Select features in dataframe
    features = list(map(lambda x : create_features(x,features_list),dates_features))
    features = [item for sublist in features for item in sublist]
    features = [item for sublist in features for item in sublist]
    features.append('churn')
    
    return sample_dataframe(df[features],sample_type)


def dataset(dataframe, nb_months_features, nb_months_label, sample_type, features_list, sliding_window):
    df = dataframe.copy()
    # Drop the monthly features that arent in features_list
    non_monthly_features = [feature for feature in list(df) if isinstance(feature,str)]
    features = [feature for feature in list(df) if feature[0] in features_list]
    df = df[features + non_monthly_features]
    
    other_features = list(df)
    datasets = []
    # *2 because of C and D
    while len(features)/len(features_list) >= (nb_months_features)*2 :
        df_test = create_dataset(df, nb_months_features, nb_months_label, sample_type, features_list)
        datasets.append(df_test)
        if sliding_window :
            date_min = min(list(zip(*[feature for feature in list(df) if not isinstance(feature,str)]))[1])
            features_to_del = [x for x in list(df) if x[1] == date_min]
            other_features = [x for x in list(df) if x not in list(features_to_del)]
        else:
            other_features = [x for x in list(df) if x not in list(df_test)]
        df = df[other_features]
        features = [feature for feature in list(df) if feature[0] in features_list]
    return np.vstack(datasets)
        
        
def sample_dataframe(df,sample_type):
    if isinstance(sample_type, float):
        df_sampled = df.sample(frac=sample_type)
    elif isinstance(sample_type, int):
        df_sampled = df.sample(n=sample_type)
    elif sample_type == 'balance':
        df_churn = df[df.churn == 1]
        len_churn = len(df_churn)
        df_sampled = df_churn.append(df[df.churn == 0].sample(n=len_churn))
    return df_sampled

def rename_column(df,nb_months_features,features_list):
    columns = []
    for i in range(nb_months_features):
        if isinstance(features_list,list) or isinstance(features_list,tuple):
            for feature in features_list:
                columns.append((feature,'month_'+str(i+1),'C'))
                columns.append((feature,'month_'+str(i+1),'D'))
        else:
            columns.append((feature,'month_'+ str(i+1),'C'))
            columns.append((feature,'month_'+ str(i+1),'D'))
    columns.append('churn')
    df.columns = columns

def plot_calibration(clf,X_test,y_test):
    X = X_test.copy()
    y = y_test.copy()
    probs = clf.predict_proba(X)[:,1]
    inds = np.digitize(probs, np.histogram(probs, bins=10 ,range=(0,1))[1])
    X["group_churn"] = inds
    X["churn"] = y
    probas = []
    for i in range(10):
        try:
            proba = float(len(X[(X.group_churn == i+1) & (X.churn == 1)])) / len(X[(X.group_churn == i+1)])
        except ZeroDivisionError:
            probas.append(0)
        else:
            probas.append(proba)
    
    plt.figure(figsize=(15,12))
    plt.plot(10*np.arange(0,1.1,0.1),np.arange(0,1.1,0.1),'r')
    plt.bar(np.arange(10),probas,width=1)
    plt.ylabel("Fréquence relative de résultat")
    plt.xticks(np.arange(11),np.arange(0,1.1,0.1))
    plt.ylim([0,1])
    plt.xlabel("Probabilité prédite")
    plt.show()
    
def plot_score(clf, X_test,y_test, plot_matrix = False):
    y = y_test.copy()
    X = X_test.copy()
    y_pred = clf.predict(X)

    print("Scores :")
    print("\nPrecision : " + str(precision_score(y_pred,y.values)*100) + ' %')
    print("Recall : " + str(recall_score(y_pred,y.values)*100) + ' %')
    print("f1: " + str(f1_score(y_pred,y.values)*100) + ' %')
    print("Accuracy: " + str(clf.score(X,y.values)*100) + ' %')

    if plot_matrix:
        def plot_confusion_matrix(cm, title='Confusion matrix', cmap=plt.cm.Blues):
            plt.imshow(cm, interpolation='nearest', cmap=cmap)
            plt.title(title)
            plt.colorbar()
            tick_marks = np.arange(2)
            plt.xticks(tick_marks, ("Non churn","Churn"))
            plt.yticks(tick_marks, ("Non churn","Churn"))
            plt.tight_layout()
            plt.ylabel('True label')
            plt.xlabel('Predicted label')
        
        cm = confusion_matrix(y, y_pred)
        print('\nConfusion matrix :\n')
        print(cm)
        plt.figure()
        plot_confusion_matrix(cm, title='Normalized confusion matrix')

        cm_normalized = cm.astype('float64') / cm.sum(axis=1)[:, np.newaxis]
        print('\nNormalized confusion matrix :\n')
        print(cm_normalized)
        plt.figure()
        plot_confusion_matrix(cm_normalized, title='Normalized confusion matrix')

def plot_features_importance(clf,X_train,columns):
    names = columns
    importances = clf.steps[1][1].feature_importances_
    indices = np.argsort(importances)[::-1]

    print('Learning dataset size : ' + str(len(X_train)))

    # Plot the feature importances of the forest
    plt.figure(figsize=(10,10))
    plt.title("Feature importances")
    plt.barh(range(X_train.shape[1]), importances[indices], color="b", align="center")
    plt.yticks(range(X_train.shape[1]), [names[i] for i in indices] ,rotation='horizontal')
    plt.ylim([-1, X_train.shape[1]])
    plt.show()

def predict_churn(df, nb_months_features,nb_months_label,features_list):
    df_test = pd.DataFrame(dataset(df, nb_months_features, nb_months_label, sample_type = 'balance',features_list = features_list, sliding_window=True))
    rename_column(df_test,nb_months_features,features_list)
    columns = list(df_test)
    X = df_test[columns[0:-1]]
    y = df_test[columns[-1]]
    columns = columns[0:-1]
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33)
    
    clf = RandomForestClassifier(class_weight='balanced',n_estimators=100)
    clf.fit(X_train, y_train)
    plot_score(clf, X_test,y_test)
